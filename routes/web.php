<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});




Auth::routes();

Route::get('login/facebook', 'Auth\SocialAuthFacebookController@redirectToFacebook');
Route::get('login/facebook/callback', 'Auth\SocialAuthFacebookController@handleFacebookCallback');

Route::get('login/google', 'Auth\SocialAuthGoogleController@redirectToGoogle');
Route::get('/callback', 'Auth\SocialAuthGoogleController@handleGoogleCallback');

Route::get('login/github', 'Auth\SocialAuthGithubController@redirectToGithub');
Route::get('/login/github/callback', 'Auth\SocialAuthGithubController@handleGithubCallback');


Route::middleware('auth')->group(function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/products/add', 'ProductController@add')->name('products.add');

    Route::resource('products', 'ProductController')->except('create');

});

