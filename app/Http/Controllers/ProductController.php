<?php

namespace App\Http\Controllers;

use App\Entity\Product;
use App\Http\Requests\ProductRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::latest()->paginate(10);

        return view('product.index', compact('products'));
    }

    public function show($id)
    {
        $product = Product::find($id);

        return view('product.show', compact('product'));
    }

    public function add()
    {
        return view('product.add');
    }

    public function store(ProductRequest $request)
    {
        Product::create([
            'name' => $request->input('name'),
            'price' => $request->input('price'),
            'user_id' => Auth::id(),
        ]);

        return redirect('/products');
    }

    public function edit($id)
    {
        $product = Product::find($id);

        if (\request()->user()->can('view', $product)) {
            return view('product.edit', compact('product'));
        } else {
            return redirect()->to('/products');
        }

    }

    public function update(ProductRequest $request, $id)
    {
        $product = Product::find($id);

        $product->name = $request->input('name');
        $product->price = $request->input('price');

        $product->save();

        return redirect('/products/'.$product->id);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('/products');
    }

}
