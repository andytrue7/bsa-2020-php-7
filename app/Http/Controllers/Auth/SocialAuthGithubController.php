<?php

namespace App\Http\Controllers\Auth;

use App\Entity\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthGithubController extends Controller
{
    public function redirectToGithub()
    {
        return Socialite::driver('github')->redirect();
    }

    public function handleGithubCallback()
    {
        $socialUser = Socialite::driver('github')->user();

        $email = $socialUser->email;

        $user = User::where('email', $email)->first();

        if(is_null($user)){
            $user = User::create([
                'name' => $socialUser->name,
                'email' => $socialUser->email,
                'password' => Hash::make(rand(1, 10000)),
            ]);
        }
        Auth::login($user);

        return redirect('/products');
    }
}
