@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Edit product</h1>
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <form action="/products/{{ $product->id }}" method="post">
                    @csrf
                    @method("PUT")

                    @error('name')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label for="name">Name:</label>
                        <input type="text" name="name" class="form-control" id="name" value="{{ $product->name }}" >
                    </div>

                    @error('price')
                    <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <div class="form-group">
                        <label for="price">Price:</label>
                        <input type="text" name="price" class="form-control" id="price" value="{{ $product->price }}">
                    </div>

                    <button type="submit" class="btn btn-primary">Save</button>

                </form>
            </div>
        </div>
    </div>
@endsection
