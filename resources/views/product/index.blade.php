@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-between">
            <h1>Product list</h1>
            <div class="col-4">
                <a href="{{ route('products.add') }}" class="btn btn-primary offset-md-4">Add product</a>
            </div>
        </div>
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                @foreach($products as $product)
                    <div class="card">
                        <div class="card-body">
                            <a href="{{ route('products.show', $product->id) }}">
                                <h3 class="card-text"> <strong>name:</strong> {{ $product->name }}</h3>
                            </a>
                            <p class="card-text"> <strong>price:</strong> {{ $product->price }}</p>
                        </div>
                    </div>
                @endforeach
            <div class="mt-3">
                {{ $products->links() }}
            </div>
            </div>
        </div>
    </div>
@endsection
