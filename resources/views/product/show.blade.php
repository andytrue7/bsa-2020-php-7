@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Product page</h1>
        <div class="row justify-content-md-center">
            <div class="col-md-6">
                <div class="card mb-3">
                    <div class="card-body d-flex justify-content-between">
                        <div>
                            <h3 class="card-text"> <strong>name:</strong> {{ $product->name }}</h3>
                            <p class="card-text"> <strong>price:</strong> {{ $product->price }}</p>
                        </div>
                        <div>
                            @can('update', $product)
                                <div class="mb-2">
                                    <a href="{{ route('products.edit', $product->id) }}" class="btn btn-warning">Edit</a>
                                </div>

                                <form action="{{ route('products.destroy', $product->id) }}" method="post">
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn badge-danger">Delete</button>
                                </form>
                            @endcan
                        </div>
                    </div>
                </div>
                <a href="{{ route('products.index') }}" class="btn btn-secondary">Back</a>
            </div>
        </div>
    </div>
@endsection
